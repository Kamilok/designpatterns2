package Builder;

public class HouseBuilder {

    public HouseBuilder(){}

    public void buildWalls() {
        System.out.println("House Wall built!");
    }


    public void buildDoors() {
        System.out.println("House Door built!");
    }


    public void buildWindows() {
        System.out.println("House Windows built!");
    }


    public void buildRoof() {
        System.out.println("House Roof built!");
    }


    public void buildGarage() {
        System.out.println("House with Garage built!");
    }


    public void buildStatue() {
        System.out.println("House with Fancy Statue built!");    }


    public void buildSwimmingPool() {
        System.out.println("House with Swimming Pool built!");    }


    public void buildGarden() {
        System.out.println("House with Garden built!");    }


    public void getResult() {
        System.out.println("House built!!!");
    }
}
