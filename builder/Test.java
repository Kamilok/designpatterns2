package Builder;

public class Test {

    public static void main(String[] args){
        HouseBuilder house = new HouseBuilder();
        house.buildWalls();
        house.buildDoors();
        house.buildWindows();
        house.buildRoof();
        house.getResult();
        house.buildGarage();
        house.buildSwimmingPool();
        house.buildStatue();
        house.buildGarden();

    }

}
