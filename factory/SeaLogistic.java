package Factory;

public class SeaLogistic extends Logistics{


    @Override
    public Transport createTransport() {
        return new Ship();
    }
}
