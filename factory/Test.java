package Factory;

public class Test {

    public static void main(String[] args){

        Logistics logistic1 = new SeaLogistic();
        logistic1.planDelivery();

        Logistics logistic2 = new RoadLogistic();
        logistic2.planDelivery();

    }

}
