package Singleton;

public class Application {

    public static void main(String[] args){

        Database db1;
        Database db2;

        db1 = Database.getInstance();
        db2 = Database.getInstance();

        if (db1 == db2){
            System.out.println("References to same Singleton object");
        }

    }

}
